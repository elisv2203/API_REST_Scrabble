# Listado de mensajes enviados via Websocket por el servidor

## Canal user.{id}

Canal privado para cada usuario, donde id es el identificador del usuario

**AcceptedChallenge**

- *Descripción*: Un solicitud de partida ha sido aceptada.

*Informacion*

|  Parámetro   |  Descripción  |
| :----------  | :------------ |
| gameId       | Identificador de la partida creada |
| lang         | Idioma de la partida |
| oppoId       | Identificador del oponente |
| oppoId       | Identificador del oponente |
| oppoName     | Nombre del oponente
| oppoCountry  | País del oponente |
| oppoAvatar   | URL del avatar del oponente |


